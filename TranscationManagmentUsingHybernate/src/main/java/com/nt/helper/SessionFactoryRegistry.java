package com.nt.helper;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class SessionFactoryRegistry {

	public static SessionFactory newSessionFactory() {
		SessionFactory  sessionFactory=null;
		if(sessionFactory==null) {
			Configuration configuration=new Configuration().configure();
			sessionFactory=configuration.buildSessionFactory();
		}
		return sessionFactory;
	}
}
