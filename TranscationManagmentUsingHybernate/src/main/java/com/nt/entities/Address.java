package com.nt.entities;

import java.io.Serializable;

public class Address implements Serializable {
	
	private static final long serialVersionUID = -4541013266437885608L;
	protected int plotNo;
	protected String laneNo;
	protected String landmark;
	protected String state;
	protected int pinCode;
	public int getPlotNo() {
		return plotNo;
	}
	public void setPlotNo(int plotNo) {
		this.plotNo = plotNo;
	}
	public String getLaneNo() {
		return laneNo;
	}
	public void setLaneNo(String laneNo) {
		this.laneNo = laneNo;
	}
	public String getLandmark() {
		return landmark;
	}
	public void setLandmark(String landmark) {
		this.landmark = landmark;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public int getPinCode() {
		return pinCode;
	}
	public void setPinCode(int pinCode) {
		this.pinCode = pinCode;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((landmark == null) ? 0 : landmark.hashCode());
		result = prime * result + ((laneNo == null) ? 0 : laneNo.hashCode());
		result = prime * result + pinCode;
		result = prime * result + plotNo;
		result = prime * result + ((state == null) ? 0 : state.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Address other = (Address) obj;
		if (landmark == null) {
			if (other.landmark != null)
				return false;
		} else if (!landmark.equals(other.landmark))
			return false;
		if (laneNo == null) {
			if (other.laneNo != null)
				return false;
		} else if (!laneNo.equals(other.laneNo))
			return false;
		if (pinCode != other.pinCode)
			return false;
		if (plotNo != other.plotNo)
			return false;
		if (state == null) {
			if (other.state != null)
				return false;
		} else if (!state.equals(other.state))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Address [plotNo=" + plotNo + ", laneNo=" + laneNo + ", landmark=" + landmark + ", state=" + state
				+ ", pinCode=" + pinCode + "]";
	}
	
	

}
