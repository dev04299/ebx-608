package com.cpk.entity;

import java.io.Serializable;

import javax.persistence.Embeddable;

import lombok.Data;

@Data
@Embeddable
public class CompositePrimaryKey implements Serializable {
	private int chairNo;
	private int tableNo;
}
