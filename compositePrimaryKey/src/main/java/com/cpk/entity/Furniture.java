package com.cpk.entity;

import java.io.Serializable;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "dabaraj")
public class Furniture implements Serializable{
	@Embedded
	private CompositePrimaryKey compositePrimaryKey;
	private String chairName;
	private String tableName;
	private String manufactureCompany;

}
