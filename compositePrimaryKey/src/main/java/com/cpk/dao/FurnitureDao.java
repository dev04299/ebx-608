package com.cpk.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.cpk.entity.CompositePrimaryKey;
import com.cpk.entity.Furniture;
import com.cpk.helper.SessionFactoryRegistry;

public class FurnitureDao {
	public void saveFurniture(Furniture furniture) {
		SessionFactory sessionFactory=null;
		Session session=null;
		boolean flag=false;
		Transaction transaction=null;

		try {
			sessionFactory=SessionFactoryRegistry.getInstance();
			session=sessionFactory.openSession();
			transaction=session.beginTransaction();

			session.save(furniture);
			flag=true;
		}finally {
			if(transaction!=null) {
				if(flag) {
					transaction.commit();
				}else {
					transaction.rollback();
				}
			}

		}
	}
	public Furniture getFurniture(int chairNo, int tableNo) {
		Furniture furniture=null;
		CompositePrimaryKey id=null;
		SessionFactory sessionFactory=null;
		Session session=null;
		try {
			sessionFactory=SessionFactoryRegistry.getInstance();
			session=sessionFactory.openSession();
			id=new CompositePrimaryKey();
			id.setChairNo(chairNo);
			id.setTableNo(tableNo);
			furniture=session.get(Furniture.class, id);
		}finally {
			if(session!=null) {
				session.close();
			}
		}
		return furniture;
	}
}
