package com.transcationManagment.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.transcationManagment.entities.LoanAccount;
import com.transcationManagment.helper.SessionFactoryRegistry;

public class LoanDao {

	public void saveLoanAccount(LoanAccount loanAccount) {
		SessionFactory sessionFactory=null;
		Session session=null;
		Transaction transaction=null;
		boolean flag=false;
		try {
			sessionFactory=SessionFactoryRegistry.newSessionFactory();
			session=sessionFactory.openSession();
			transaction=session.beginTransaction();
			
			session.save(loanAccount);
			flag=true;
		}finally {
			if(transaction!=null) {
				if(flag) {
					transaction.commit();
				}else {
					transaction.rollback();
					
				}
			}
			if(session!=null) {
				session.close();
			}
		}
	}
}
