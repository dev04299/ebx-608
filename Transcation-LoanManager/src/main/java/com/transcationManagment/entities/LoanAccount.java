package com.transcationManagment.entities;

import java.io.Serializable;

public class LoanAccount implements Serializable{
	

	private static final long serialVersionUID = -1096540152116045376L;

	protected int loanNo;
	protected int loanAccountNo;
	protected String loanAccountHolderName;
	protected int tenure;
	public int getLoanNo() {
		return loanNo;
	}
	public void setLoanNo(int loanNo) {
		this.loanNo = loanNo;
	}
	public int getLoanAccountNo() {
		return loanAccountNo;
	}
	public void setLoanAccountNo(int loanAccountNo) {
		this.loanAccountNo = loanAccountNo;
	}
	public String getLoanAccountHolderName() {
		return loanAccountHolderName;
	}
	public void setLoanAccountHolderName(String loanAccountHolderName) {
		this.loanAccountHolderName = loanAccountHolderName;
	}
	public int getTenure() {
		return tenure;
	}
	public void setTenure(int tenure) {
		this.tenure = tenure;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((loanAccountHolderName == null) ? 0 : loanAccountHolderName.hashCode());
		result = prime * result + loanAccountNo;
		result = prime * result + loanNo;
		result = prime * result + tenure;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LoanAccount other = (LoanAccount) obj;
		if (loanAccountHolderName == null) {
			if (other.loanAccountHolderName != null)
				return false;
		} else if (!loanAccountHolderName.equals(other.loanAccountHolderName))
			return false;
		if (loanAccountNo != other.loanAccountNo)
			return false;
		if (loanNo != other.loanNo)
			return false;
		if (tenure != other.tenure)
			return false;
		return true;
	}
	
	
	
}
