package com.transcationManagment.helper;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class SessionFactoryRegistry {

	private static SessionFactory sessionFactory=null;

	static {
		Configuration configuration=new Configuration().configure();
		sessionFactory=configuration.buildSessionFactory();
	}
	public static SessionFactory newSessionFactory() {
		return sessionFactory;
	}
	public static void closeFactory() {
		if(sessionFactory!=null) {
			sessionFactory.close();
		}
	}
}
