package com.transcationManagment.test;

import com.transcationManagment.dao.LoanDao;
import com.transcationManagment.entities.LoanAccount;
import com.transcationManagment.helper.SessionFactoryRegistry;

public class LoanAccountTest {

	public static void main(String[] args) {

		try {
			LoanDao loanDao=new LoanDao();
			LoanAccount loanAccount=new LoanAccount();
			loanAccount.setLoanAccountNo(23456);
			loanAccount.setLoanAccountHolderName("DEBARAJ MAHAPATRA");
			loanAccount.setLoanNo(23);
			loanAccount.setTenure(10);

			loanDao.saveLoanAccount(loanAccount);
			System.out.println("loan account saved");
		}finally {
			SessionFactoryRegistry.closeFactory();
		}
	}
}
