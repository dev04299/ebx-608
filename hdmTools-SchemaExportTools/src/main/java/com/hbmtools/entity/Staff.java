package com.hbmtools.entity;

import java.io.Serializable;

import lombok.Data;

@Data
public class Staff implements Serializable {

	
	private static final long serialVersionUID = -7691826489980176809L;
	
	protected int staffNo;
	protected String staffName;
	protected String mobileNo;
	protected String address;
	
	
}
