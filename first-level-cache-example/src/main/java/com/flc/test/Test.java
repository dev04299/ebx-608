package com.flc.test;

import com.flc.dao.CustomerDao;
import com.flc.helper.SessionFactoryRegistry;

public class Test {

	public static void main(String[] args) {
		try {
			CustomerDao dao=new CustomerDao();
			dao.getCustomerDetails(1);
		}finally {
			SessionFactoryRegistry.close();
		}
	}
}
