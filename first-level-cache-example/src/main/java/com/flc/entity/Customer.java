package com.flc.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name= "custo")
@Data
public class Customer {

	@Id
	@Column(name= "cus_No")
	private int cusNo;
	@Column(name= "cus_Name")
	private String cusName;
	@Column(name="cus_Mobile_No")
	private String cusMobileNo;
	private String address;
}
