package com.flc.helper;

import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataBuilder;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.SessionFactoryBuilder;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

public class SessionFactoryRegistry {

	private static SessionFactory instance;
	
	static {
		StandardServiceRegistryBuilder builder=new StandardServiceRegistryBuilder().configure();
		//add initiator
		StandardServiceRegistry registry=builder.build();
		
		MetadataSources metadataSources=new MetadataSources(registry);
		MetadataBuilder metadataBuilder=metadataSources.getMetadataBuilder();
		Metadata metadata=metadataBuilder.build();
		SessionFactoryBuilder sessionFactoryBuilder=metadata.getSessionFactoryBuilder();
		
		sessionFactoryBuilder.applyAutoClosing(true);
		instance=sessionFactoryBuilder.build();
		System.out.println(instance);
	}
	
	public static SessionFactory getInstnce() {
		return instance;
	}
	
	public static void close() {
		if(instance!=null) {
			instance.close();
		}
	}
	
}
