package com.flc.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.flc.entity.Customer;
import com.flc.helper.SessionFactoryRegistry;

public class CustomerDao {

	public Customer getCustomerDetails(int customerNo) {
		Session session=null;
		Session session2=null;
		SessionFactory sessionFactory=null;
		Customer customer, customer1=null;
		
		try {
			sessionFactory=SessionFactoryRegistry.getInstnce();
			session=sessionFactory.openSession();
			customer=session.get(Customer.class, 2);
			customer1=session.get(Customer.class, 12);
			
			System.out.println( "customer == customer1 ?  " +(customer==customer1));
			System.out.println(customer);
			System.out.println(customer1);
		}finally {
			if(session!=null) {
				session.close();
			}
		}
		
		return customer;
	}
}
