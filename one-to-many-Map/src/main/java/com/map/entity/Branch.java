package com.map.entity;

import java.io.Serializable;
import java.util.Map;

import lombok.Data;

@Data
public class Branch implements Serializable{

	private static final long serialVersionUID = -7072380019659624238L;
	protected int branchNo;
	protected String branchName;
	protected String address;
	protected long contactNo;
	Map<String, Account> assignedAccounts;

}
