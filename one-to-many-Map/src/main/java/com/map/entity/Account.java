package com.map.entity;

import java.io.Serializable;

import lombok.Data;
@Data
public class Account implements Serializable{
	
	private static final long serialVersionUID = -3842767846888330028L;
	protected int accountNo;
	protected String accountHolderName;
	protected String ifscCode;
	protected String address;
	protected double phoneNo;
	

}
