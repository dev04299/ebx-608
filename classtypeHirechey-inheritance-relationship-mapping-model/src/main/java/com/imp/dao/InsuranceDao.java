package com.imp.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.imp.entity.InsurancePlan;
import com.imp.helper.SessionFactoryRegistry;

public class InsuranceDao {

	public int saveInsurance(InsurancePlan insurancePlan) {
		int planNo=0;
		SessionFactory sessionFactory=null;
		Session session=null;
		Transaction transaction=null;
		boolean flag=false;

		try {
			sessionFactory=SessionFactoryRegistry.getInstance();
			session=sessionFactory.openSession();
			transaction=session.beginTransaction();

			planNo=(int) session.save(insurancePlan);
			System.out.println("plan no :: " + planNo);
			flag=true;
		}finally {
			if(transaction != null) {
				if(flag) {
					transaction.commit();
				}else {
					transaction.rollback();
				}
			}

			if(session!=null) {
				session.close();
			}

		}
		return planNo;
	}
	public InsurancePlan getInsuranceDetails(Class<?> entityClass, int planNo) {
		InsurancePlan insurancePlan=null;
		Session session=null;
		SessionFactory sessionFactory=null;

		try {
			sessionFactory=SessionFactoryRegistry.getInstance();
			session=sessionFactory.openSession();

			insurancePlan=(InsurancePlan) session.get(entityClass, planNo);
		}finally {
			if(session != null) {
				session.close();
			}
		}
		return insurancePlan;
	}

}
