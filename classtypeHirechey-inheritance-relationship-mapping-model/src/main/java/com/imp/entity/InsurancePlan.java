package com.imp.entity;

import java.io.Serializable;

import lombok.Data;

@Data
public class InsurancePlan implements Serializable{
	
	
	private static final long serialVersionUID = -8740822381987129432L;
	protected int planNo;
	protected String planName;
	protected int minTenure;
	protected int maxTenure;
	protected String desciption;
	
	
}
