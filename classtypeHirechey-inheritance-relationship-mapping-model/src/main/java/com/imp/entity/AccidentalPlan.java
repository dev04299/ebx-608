package com.imp.entity;

import lombok.Data;

@Data
public class AccidentalPlan extends InsurancePlan{

	private static final long serialVersionUID = 6316910749268188394L;
	protected String placeType;
	protected String accidentalCause;
}
