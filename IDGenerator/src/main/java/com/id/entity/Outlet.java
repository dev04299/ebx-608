package com.id.entity;

import lombok.Data;

@Data
public class Outlet {

	private String outletNo;
	private String outletName;
	private String description;
	private float price;
}
