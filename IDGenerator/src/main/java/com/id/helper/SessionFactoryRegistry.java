package com.id.helper;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

public class SessionFactoryRegistry {
	
	private static SessionFactory sessionFactory;
	static {
		Configuration configuration=new Configuration();
		StandardServiceRegistryBuilder builder=new StandardServiceRegistryBuilder().configure();
		StandardServiceRegistry registry=builder.build();
		sessionFactory=configuration.buildSessionFactory(registry);
	}
	public static SessionFactory getSessionfactory() {
		return sessionFactory;
	}
	public static void closeSessionFactory() {
		if(sessionFactory!=null) {
			sessionFactory.close();
		}
	}
}
