package com.id.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.id.entity.Outlet;
import com.id.helper.SessionFactoryRegistry;

public class OutletDao {

	public String insertIntoOutlet(Outlet outlet) {
		String outletNo=null;
		SessionFactory sessionFactory=null;
		Session session=null;
		boolean flag=false;
		Transaction transaction=null;

		try {
			sessionFactory=SessionFactoryRegistry.getSessionfactory();
			session=sessionFactory.openSession();
			transaction=session.beginTransaction();
			outletNo=(String) session.save(outlet);
			flag=true;
		}finally {
			if(transaction!=null) {
				if(flag) {
					transaction.commit();
				}else {
					transaction.rollback();
				}
			}
			if(session!=null) {
				session.close();
			}
		}


		return outletNo;
	}

}
