package com.subclasstype.entity;

import lombok.Data;

@Data
public class AccidentalInsurancePlan extends InsurancePlanner{

	private String vechileType;
	private String causeOfAccident;
	
}
