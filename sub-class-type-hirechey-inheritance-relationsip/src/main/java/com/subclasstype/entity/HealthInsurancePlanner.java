package com.subclasstype.entity;

import lombok.Data;

@Data
public class HealthInsurancePlanner extends InsurancePlanner{
	private String disabilityCoverage;
	private  String internationalCoverage;
}
