package com.subclasstype.entity;

import lombok.Data;

@Data
public class InsurancePlanner {
	private int planNo;
	private String planName;
	private int minEligibilityAge;
	private int maxEligibilityAge;
	private int minTenure;
	private int maxTenure;
}
