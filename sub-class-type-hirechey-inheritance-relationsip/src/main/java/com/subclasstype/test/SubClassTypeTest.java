package com.subclasstype.test;

import com.subclasstype.dao.InsurancePlannerDao;
import com.subclasstype.entity.AccidentalInsurancePlan;
import com.subclasstype.entity.HealthInsurancePlanner;
import com.subclasstype.entity.InsurancePlanner;
import com.subclasstype.helper.SessionFactoryRegostry;

public class SubClassTypeTest {

	public static void main(String[] args) {
		try {
			InsurancePlannerDao dao=new InsurancePlannerDao();
//			InsurancePlanner insurancePlanner=new InsurancePlanner();
//			insurancePlanner.setPlanName("jeevan surakhya Yojana");
//			insurancePlanner.setMinTenure(35000);
//			insurancePlanner.setMaxTenure(90000);
//			insurancePlanner.setMinEligibilityAge(18);
//			insurancePlanner.setMaxEligibilityAge(45);
//			dao.saveInsurancePlanner(insurancePlanner);
//
//			HealthInsurancePlanner hip=new HealthInsurancePlanner();
//			hip.setPlanName("Medical Insurance Plan");
//			hip.setMinTenure(35000);
//			hip.setMaxTenure(90000);
//			hip.setMinEligibilityAge(18);
//			hip.setMaxEligibilityAge(45);
//			hip.setDisabilityCoverage("true");
//			hip.setInternationalCoverage("international policy will be applied");
//
//			dao.saveInsurancePlanner(hip);
//
//			AccidentalInsurancePlan aip=new AccidentalInsurancePlan();
//			aip.setPlanName("road safety Yojana");
//			aip.setMinTenure(35000);
//			aip.setMaxTenure(90000);
//			aip.setMinEligibilityAge(18);
//			aip.setMaxEligibilityAge(45);
//			aip.setCauseOfAccident("bike accident");
//			aip.setVechileType("2 wheeler");
//
//			dao.saveInsurancePlanner(aip);
			InsurancePlanner insurancePlanner=dao.getInsuranceDetails(InsurancePlanner.class, 3);
			System.out.println(insurancePlanner);
		}finally {
			SessionFactoryRegostry.close();
		}


	}

}
