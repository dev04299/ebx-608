package com.subclasstype.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.subclasstype.entity.InsurancePlanner;
import com.subclasstype.helper.SessionFactoryRegostry;

public class InsurancePlannerDao {

	public int saveInsurancePlanner(InsurancePlanner insurancePlanner) {
		int planNo=0;
		SessionFactory sessionFactory=null;
		Session session=null;
		Transaction transaction=null;
		boolean flag=false;
		
		try {
			sessionFactory=SessionFactoryRegostry.getInstance();
			session=sessionFactory.openSession();
			transaction=session.beginTransaction();
			planNo=(int) session.save(insurancePlanner);
			
			System.out.println("plan no ::" +planNo);
			flag=true;
		}finally {
			if(transaction != null) {
				if(flag) {
					transaction.commit();
				}else {
					transaction.rollback();
				}
			}
			if(session != null) {
				session.close();
			}
		}
		return planNo;
	}
	public InsurancePlanner getInsuranceDetails(Class<?> entityClass, int planNo) {
		InsurancePlanner insurancePlanner=null;
		Session session=null;
		SessionFactory sessionFactory=null;

		try {
			sessionFactory=SessionFactoryRegostry.getInstance();
			session=sessionFactory.openSession();

			insurancePlanner=(InsurancePlanner) session.get(entityClass, planNo);
		}finally {
			if(session != null) {
				session.close();
			}
		}
		return insurancePlanner;
	}

}
