package com.tm.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.tm.entities.Account;
import com.tm.entities.Address;

public class UserAccountDao {

	private static final String insert_Query_account="insert into account(account_no, account_holder_nm, address, mobile, email_id) values(?,?,?,?,?)";
	private static final String insert_Query_address="insert into address(plot_no, lane_no, landmark, state, pin_code) values(?,?,?,?,?)";

	public void registration(Account account, Address address) throws SQLException {


		Connection con=null;
		PreparedStatement psAccount=null;
		PreparedStatement psAddress=null;
		boolean flag=false;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			con=DriverManager.getConnection("jdbc:mysql:///con2","root","root");
			con.setAutoCommit(false);
			psAccount=con.prepareStatement(insert_Query_account);
			//set the query param
			psAccount.setInt(1, account.getAccoutnNo());
			psAccount.setString(2, account.getAccountHolderName());
			psAccount.setString(3, account.getAddress());
			psAccount.setString(4, account.getMobile());
			psAccount.setString(5, account.getEmailId());

			psAccount.execute();

			psAddress=con.prepareStatement(insert_Query_address);
			//set the query params
			psAddress.setInt(1, address.getPlotNo());
			psAddress.setString(2,address.getLaneNo());
			psAddress.setString(3, address.getLandmark());
			psAddress.setString(4, address.getState());
			psAddress.setInt(5, address.getPinCode());

			psAddress.execute();
			System.out.println("Registarion Successfully ....!!!");

			flag=true;
		}catch (ClassNotFoundException e) {
			System.out.println("Driver Name not found " +e);
		}
		catch(SQLException se) {
			System.out.println("you are facing sql exception... Plz check in SQL syntax" +se);
		}finally {
			if(con!=null) {
				if(flag) {
					System.out.println("You dont have any Problem ...");
					con.commit();
					
				}else {
					System.out.println("you are having problem...");
					con.rollback();
				}
			}
		}

	}
}
