package com.tm.entities;

public class Account {

	protected int accoutnNo;
	protected String accountHolderName;
	protected String address;
	protected String mobile;
	protected String emailId;
	public int getAccoutnNo() {
		return accoutnNo;
	}
	public void setAccoutnNo(int accoutnNo) {
		this.accoutnNo = accoutnNo;
	}
	public String getAccountHolderName() {
		return accountHolderName;
	}
	public void setAccountHolderName(String accountHolderName) {
		this.accountHolderName = accountHolderName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	
	
	
	
}
