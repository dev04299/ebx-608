package com.tm.test;

import java.sql.SQLException;

import com.tm.dao.UserAccountDao;
import com.tm.entities.Account;
import com.tm.entities.Address;

public class TranscationManagmentTest {

	Account account;
	Address address;
	
	public static void main(String[] args) throws SQLException {
		UserAccountDao userAccountDao=new UserAccountDao();
		Account account=new Account();
		account.setAccountHolderName("Bikram");
		account.setAccoutnNo(2344768);
		account.setAddress("Palasuni");
		account.setEmailId("devmadhav@gmail.com");
		account.setMobile("989087345");

		Address address=new Address();
		address.setPlotNo(36798);
		address.setLandmark("jharapada");
		address.setLaneNo("2");
		address.setPinCode(751006);
		address.setState("orissa");
		userAccountDao.registration(account, address);
	
	}
}
