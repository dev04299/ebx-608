package com.annotation.dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import com.annotation.entity.Bike;
import com.annotation.helper.EMFRegistry;

public class BikeDao {

	public Bike getBikeDetails(int bikeNo) {
		EntityManagerFactory emf=null;
		EntityManager em=null;
		Bike bike=null;
		try {
			emf=EMFRegistry.createEniEntityManagerFactory();
			em=emf.createEntityManager();
			bike=em.find(Bike.class, bikeNo);
		}finally {
			if(em!=null) {
				em.close();
			}
		}
		return bike;

	}
}
