package com.annotation.test;

import com.annotation.dao.BikeDao;
import com.annotation.entity.Bike;
import com.annotation.helper.EMFRegistry;

public class AnnotationJPATest {

	public static void main(String[] args) {

		try {
			BikeDao dao=new BikeDao();
			Bike bike=dao.getBikeDetails(1);
			System.out.println("Bike details are :: "+bike);
		}finally {
			EMFRegistry.close();
		}
	}
}
