package com.annotation.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "bike")
@Setter
@Getter
@ToString
@EqualsAndHashCode
public class Bike implements Serializable{
	private static final long serialVersionUID = 1256125L;
	@Id
	@Column(name = "bike_no")
	protected int bikeNo;	
	@Column(name="bike_name")
	protected String bikeName;
	protected String type;
	protected String company;
	@Column(name="warranty_period")
	protected String warrantyPeriod;
	

}
