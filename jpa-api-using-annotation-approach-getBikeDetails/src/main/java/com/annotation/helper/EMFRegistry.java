package com.annotation.helper;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class EMFRegistry {

	private static EntityManagerFactory emf;
	static {
		emf=Persistence.createEntityManagerFactory("deba");
	}
	public static EntityManagerFactory createEniEntityManagerFactory() {
		return emf;
	}
	public static void close() {
		if(emf!=null) {
			emf.close();
		}
	}
}
