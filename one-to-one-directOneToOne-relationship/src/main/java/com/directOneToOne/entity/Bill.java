package com.directOneToOne.entity;

import lombok.Data;

@Data
public class Bill {
	protected int billNo;
	protected String billHolderName;
	protected String billreferenceNo;
	protected double amount;
}
