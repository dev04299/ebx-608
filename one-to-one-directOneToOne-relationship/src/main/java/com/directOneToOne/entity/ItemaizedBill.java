package com.directOneToOne.entity;

import lombok.Data;

@Data
public class ItemaizedBill {
	protected int billNo;
	protected String billName;
	protected double electrictyBillAmount;
	protected double phoneBillAmount;
	protected String feedBack;
	protected Bill bill;
	
}
