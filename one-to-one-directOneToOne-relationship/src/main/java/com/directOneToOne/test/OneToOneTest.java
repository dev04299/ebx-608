package com.directOneToOne.test;

import com.directOneToOne.dao.ItemaizedBillDao;
import com.directOneToOne.entity.Bill;
import com.directOneToOne.entity.ItemaizedBill;
import com.directOneToOne.helper.SessionFactoryRegistry;

public class OneToOneTest {
	public static void main(String[] args) {
		ItemaizedBillDao dao=new ItemaizedBillDao();
		Bill bill=new Bill();
		bill.setBillHolderName("madhav ranjan reddy");
		bill.setBillreferenceNo("ELE_785623");
		bill.setAmount(23443);
		
		ItemaizedBill iBill=new ItemaizedBill();
		iBill.setBillName("Home Bill");
		iBill.setElectrictyBillAmount(1544);
		iBill.setPhoneBillAmount(456);
		iBill.setFeedBack("good");
		iBill.setBill(bill);
	
		int iBillNo=dao.saveItemaizedBill(iBill);
		System.out.println("Itemazied Bill No is :: " +iBillNo);
		SessionFactoryRegistry.close();
	}
}
