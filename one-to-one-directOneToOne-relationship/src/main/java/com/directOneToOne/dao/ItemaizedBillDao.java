package com.directOneToOne.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.directOneToOne.entity.ItemaizedBill;
import com.directOneToOne.helper.SessionFactoryRegistry;

public class ItemaizedBillDao {

	public int saveItemaizedBill(ItemaizedBill itemaizedBill) {
		int itemaziedBillNo=0;
		SessionFactory sessionFactory=null;
		Session session=null;
		Transaction transaction=null;
		boolean flag=false;
		try {
			sessionFactory=SessionFactoryRegistry.getInstance();
			session=sessionFactory.openSession();
			transaction=session.beginTransaction();
			
			itemaziedBillNo=(int) session.save(itemaizedBill);
			System.out.println("ItemaziedBill No is  :: " +itemaziedBillNo);

			flag=true;
		}finally {
			if(transaction != null) {
				if(flag) {
					transaction.commit();
				}else {
					transaction.rollback();
				}
			}
		}
		return itemaziedBillNo;
	}
}
