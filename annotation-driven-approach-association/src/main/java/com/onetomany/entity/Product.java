package com.onetomany.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Entity
@Table(name = "product")
public class Product implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -2174787274608712056L;
	@Id
	@Column(name= "product_no")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	protected int productNo;
	@Column(name = "product_nm")
	protected String productName;
	@Column(name = "desc")
	protected String description;
	@Column(name = "food_type")
	protected String foodType;
	protected int price;
	
	@ManyToOne
	@JoinColumn(name = "company_reg_no", nullable = false)
	protected Manufacture manufacture;
}
