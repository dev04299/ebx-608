package com.onetomany.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name="manufacture")
public class Manufacture implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -2322275164804706097L;
	@Id
	@Column(name="company_reg_no")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	protected int companyRegNo;
	@Column(name = "company_nm")
	protected String companyName;
	@Column(name = "reg_year")
	protected int regYear;
	
	@OneToMany(mappedBy = "manufacture", fetch = FetchType.EAGER)
	protected Set<Product> products;

	
}
