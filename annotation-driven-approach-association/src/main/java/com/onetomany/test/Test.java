package com.onetomany.test;

import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;

import com.onetomany.entity.Manufacture;
import com.onetomany.entity.Product;
import com.onetomany.helper.SessionFactoryRegistry;

public class Test {

	public static void main(String[] args) {
		EntityManagerFactory emf=null;
		EntityManager entityManager=null;
		EntityTransaction entityTransaction=null;
		boolean flag=false;
		Product product1=null;
		Product product2=null;
		
		Manufacture manufacture=null;
		Set<Product> products=null;
		
		try {
			emf=SessionFactoryRegistry.getInstnce();
			entityManager=emf.createEntityManager();
			entityTransaction=entityManager.getTransaction();
			entityTransaction.begin();
			
			System.out.println("Transcation begin....");
			
			manufacture=new Manufacture();
			manufacture.setCompanyRegNo(231);
			manufacture.setCompanyName("RVG");
			manufacture.setRegYear(1933);
			entityManager.persist(manufacture);
			
			product1=new Product();
			product1.setProductNo(12);
			product1.setProductName("sampoo");
			product1.setPrice(1);
			product1.setDescription("for hair cleaning");
			product1.setFoodType("foo hair purpose");
			product1.setManufacture(manufacture);
			entityManager.persist(product1);
			
			product2=new Product();
			product2.setProductNo(11);
			product2.setProductName("Biskut");
			product2.setPrice(1);
			product2.setDescription("for eat ");
			product2.setFoodType("food");
			product2.setManufacture(manufacture);
			entityManager.persist(product2);
			
			/*
			 * products=new HashSet<Product>(); products.add(product1);
			 * products.add(product2);
			 */
			
		
//			manufacture.setProducts(products);
			
			//entityManager.persist(manufacture);
			
			flag=true;
		}finally {
			if(entityTransaction!=null) {
				if(flag) {
					entityTransaction.commit();
				}else {
					entityTransaction.rollback();
				}
			}
		}
	}

}
