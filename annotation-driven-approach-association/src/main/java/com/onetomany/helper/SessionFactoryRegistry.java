package com.onetomany.helper;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class SessionFactoryRegistry {
	private static EntityManagerFactory emf;
	
	static {
		emf=Persistence.createEntityManagerFactory("anno-pu");
	}
	public static EntityManagerFactory getInstnce() {
		return emf;
	}
	public static void close() {
		if(emf!=null) {
			emf.close();
		}
	}
}
