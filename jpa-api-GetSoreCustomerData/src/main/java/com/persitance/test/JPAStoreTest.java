package com.persitance.test;

import com.persitance.dao.StoreDao;
import com.persitance.entity.Store;
import com.persitance.helper.EMFRegistry;

public class JPAStoreTest {
	public static void main(String[] args) {
		try {
		StoreDao dao=new StoreDao();
		Store store=dao.getStore(1);
		System.out.println(store);
		}finally {
			EMFRegistry.close();
		}
	}

}
