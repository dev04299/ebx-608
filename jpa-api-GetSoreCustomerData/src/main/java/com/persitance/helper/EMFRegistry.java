package com.persitance.helper;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class EMFRegistry {

	private static EntityManagerFactory emf;
	static {
		emf=Persistence.createEntityManagerFactory("store-pu");
	}
	public static EntityManagerFactory createEntityManagerFactory() {
		return emf;
	}
	public static void close() {
		System.out.println("EMFRegistry.close()");
		if(emf!=null) {
			emf.close();
		}
	}
}
