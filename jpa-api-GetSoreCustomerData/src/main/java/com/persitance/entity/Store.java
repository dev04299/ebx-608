package com.persitance.entity;

import java.io.Serializable;
import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode
@ToString
public class Store implements Serializable{
	
	private static final long serialVersionUID = 6009946797277904740L;
	protected int merchantNo;
	protected String merchantNm;
	protected String items;
	protected String mobileNo;
	protected String address;
	protected String district;
	protected String state;
	protected String pincode;
	

}
