package com.persitance.dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;

import com.persitance.entity.Store;
import com.persitance.helper.EMFRegistry;

public class StoreDao {

	public Store getStore(int merchantNo) {
		EntityManagerFactory emf=null;
		EntityManager em=null;
		Store store=null;
		EntityTransaction ent=null;
		boolean flag=false;
		try {
			emf=EMFRegistry.createEntityManagerFactory();
			em=emf.createEntityManager();
			em.getTransaction();
			store=em.find(Store.class, merchantNo);
			store.setMerchantNm("madhav");
			em.persist(store);
			flag=true;
		}finally {
			if(ent!=null) {
				if(flag) {
					ent.commit();
				}else {
					ent.rollback();
				}
			}
			if(em!=null) {
				em.close();
			}
		}
		return store;
		
	}
}
