package com.manyToMany.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.manyToMany.entity.Channel;
import com.manyToMany.helper.SessionFactoryRegistry;

public class ChannelDao {
 
	public int saveChannel(Channel channel) {
		int channelNo=0;
		SessionFactory sessionFactory=null;
		Session session=null;
		Transaction transaction=null;
		boolean flag=false;
		try {
			sessionFactory=SessionFactoryRegistry.getInstance();
			session=sessionFactory.openSession();
			transaction=session.beginTransaction();
			channelNo=(int) session.save(channel);
			flag=true;
		}finally {
			if(transaction!=null) {
				if(flag) {
					transaction.commit();
				}else {
					transaction.rollback();
				}
			}
		}
		
		return channelNo;
	}
}
