package com.manyToMany.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.manyToMany.entity.DthPackageService;
import com.manyToMany.helper.SessionFactoryRegistry;

public class PackageDao {

	public int savePackage(DthPackageService dthPackageService) {
		int packageNo=0;
		SessionFactory sessionFactory=null;
		Session session=null;
		Transaction transaction=null;
		boolean flag=false;
		
		try {
			sessionFactory=SessionFactoryRegistry.getInstance();
			session=sessionFactory.openSession();
			transaction=session.beginTransaction();
			
			packageNo=(int) session.save(dthPackageService);
			System.out.println("package no :: " +packageNo);
			
			
			flag=true;
		}finally {
			if(transaction!=null) {
				if(flag) {
					transaction.commit();
				}else {
					transaction.rollback();
				}
			}
		}
		return packageNo;
	}
}
