package com.manyToMany.entity;

import java.io.Serializable;
import java.util.Set;

import lombok.Data;

@Data
public class DthPackageService implements Serializable{
	
	private static final long serialVersionUID = 7909744268895076419L;
	protected int packageNo;
	protected String packageName;
	protected String region;
	protected double price;
	protected Set<Channel> channels;
}
