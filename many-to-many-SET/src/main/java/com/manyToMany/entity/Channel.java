package com.manyToMany.entity;

import java.io.Serializable;

import lombok.Data;

@Data
public class Channel implements Serializable{
	
	private static final long serialVersionUID = -8203558404522935643L;
	protected int channelNo;
	protected String channelName;
	protected double subscriptonPack;

}
