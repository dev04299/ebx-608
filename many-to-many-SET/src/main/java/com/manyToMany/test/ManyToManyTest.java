package com.manyToMany.test;

import java.util.HashSet;
import java.util.Set;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.manyToMany.dao.ChannelDao;
import com.manyToMany.dao.PackageDao;
import com.manyToMany.entity.Channel;
import com.manyToMany.entity.DthPackageService;
import com.manyToMany.helper.SessionFactoryRegistry;

public class ManyToManyTest {
	public static void main(String[] args) {
		/*PackageDao dao=new PackageDao();
		ChannelDao cdao=new ChannelDao();
		 */		
		SessionFactory sessionFactory=null;
		Session session=null;
		Transaction transaction=null;
		boolean flag=false;
		Channel channel1=null;
		Channel channel2=null;
		Set<Channel> channels=null;
		DthPackageService dthPackageService=null;
		try {
			sessionFactory=SessionFactoryRegistry.getInstance();
			session=sessionFactory.openSession();
			transaction=session.beginTransaction();
//			channel1=new Channel();
//			channel2=new Channel();
//
//			channel1.setChannelName("TRARANGA CHANNEL");
//			channel1.setSubscriptonPack(17);
//			int channelNo1=(int) session.save(channel1);
//			System.out.println("channel 1 ::::::::::::::::" +channelNo1);
//
//			channel2.setChannelName("OTV NEWS");
//			channel2.setSubscriptonPack(17);
//			int channelNo2=(int) session.save(channel2);
//			System.out.println("channel 2 :::::::::::::::: " +channelNo2);
//
//			channels=new HashSet<Channel>();
//			channels.add(channel1);
//			channels.add(channel2);
//
//			dthPackageService=new DthPackageService();
//			dthPackageService.setPackageName("ODIA PACKAGE");
//			dthPackageService.setRegion("odisha");
//			dthPackageService.setPrice(17);
//			dthPackageService.setChannels(channels);
//
//			int packageNo=(int) session.save(dthPackageService);
//			System.out.println("package no :: " +packageNo);

			
			dthPackageService=session.get(DthPackageService.class, 1);
			System.out.println(dthPackageService);
			flag=true;
		}finally {
			if(transaction!=null) {
				if(flag) {
					transaction.commit();
				}else {
					transaction.rollback();
				}
			}
		}

	}
}
