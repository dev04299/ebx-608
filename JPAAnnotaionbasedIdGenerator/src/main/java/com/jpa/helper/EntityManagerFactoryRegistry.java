package com.jpa.helper;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class EntityManagerFactoryRegistry {
	private static EntityManagerFactory emf;
	
	static {
		emf=Persistence.createEntityManagerFactory("deba");
	}
	public static EntityManagerFactory getInstance() {
		return emf;
	}
	public static void close() {
		if(emf!=null) {
			emf.close();
		}
	}
}
