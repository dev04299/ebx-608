package com.jpa.dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;

import com.jpa.entity.Bill;
import com.jpa.helper.EntityManagerFactoryRegistry;

public class BillDao {
	
	public int saveBill(Bill bill) {
		int billNo=0;
		boolean flag=false;
		EntityTransaction et=null;
		EntityManager em=null;
		EntityManagerFactory emf=null;
		
		try {
			emf=EntityManagerFactoryRegistry.getInstance();
			em=emf.createEntityManager();
			et=em.getTransaction();
			et.begin();
			
			em.persist(bill);
			billNo=bill.getBillNo();
			System.out.println("Bill no is :: " +billNo);
			
			flag=true;
		}finally {
			if(et != null){
				if(flag) {
					et.commit();
				}else {
					et.rollback();
				}
			}
			if(em != null) {
				em.close();
			}
		}
		
		
		return billNo;
		
	}
}
