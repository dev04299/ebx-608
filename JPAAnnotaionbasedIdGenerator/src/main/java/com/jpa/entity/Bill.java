package com.jpa.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import lombok.Data;

@Data
@Entity
@Table(name="bill")
public class Bill {
	
	@Id
	@Column(name ="bill_no")
	@TableGenerator(name ="billgen", table="unique_keys", pkColumnName = "sl_no", pkColumnValue = "value", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "billgen")
	private int billNo;
	@Column(name = "billing_holder_nm")
	private String billingHolderName;
	@Column(name = "billing_address")
	private String billingAddress;
	private float price;

}
