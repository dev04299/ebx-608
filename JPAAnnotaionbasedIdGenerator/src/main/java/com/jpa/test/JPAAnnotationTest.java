package com.jpa.test;

import com.jpa.dao.BillDao;
import com.jpa.entity.Bill;

public class JPAAnnotationTest {

	public static void main(String[] args) {
		BillDao dao=new BillDao();
		
		Bill bill=new Bill();
		bill.setBillingHolderName("debaraj");
		bill.setBillingAddress("BBSR");
		bill.setPrice(500);
		dao.saveBill(bill);
	}
}
