package com.fetch.helper;

import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

public class SessionFactoryRegistry {

	private static SessionFactory instance;

	static {
		instance=new MetadataSources(new StandardServiceRegistryBuilder().configure().build()	).buildMetadata().buildSessionFactory();
	}
	public static SessionFactory getInstance() {
		return instance;
	}
	public static void close() {
		if(instance!=null) {
			instance.close();
		}
	}
}
