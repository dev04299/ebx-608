package com.fetch.test;

import java.util.HashSet;
import java.util.Set;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.fetch.entity.Office;
import com.fetch.entity.Staff;
import com.fetch.helper.SessionFactoryRegistry;


public class FetchTest {

	public static void main(String[] args) {
		SessionFactory sessionFactory=null;
		Session session=null;
		Transaction transcation=null;
		boolean flag=false;
		Staff staff1=null;
		Staff staff2=null;
		Office office=null;
		
		try {
			sessionFactory=SessionFactoryRegistry.getInstance();
			session=sessionFactory.openSession();
			transcation=session.beginTransaction();
			Set<Staff> staffSet=null;
			
			
			office=new Office();
			office.setOfficeName("RVG");
			office.setRegYear(1997);
			office.setOfficeAddress("Jharapada, BBSR");
			
			staff1=new Staff();
			staff1.setStaffFullName("Debaraj Mahapatra");
			staff1.setMobileNo("8908923881");
			staff1.setExperiences(2);
			staff1.setAddress("BBSR");
			session.save(staff1);
			
			staff2=new Staff();
			staff2.setStaffFullName("Saishree Kar");
			staff2.setMobileNo("9938855040");
			staff2.setExperiences(3);
			staff2.setAddress("RASULGARD");
			session.save(staff2);
			
			staffSet=new HashSet<>();
			staffSet.add(staff1);
			staffSet.add(staff2);
				
			office.setStaffs(staffSet);
			
			session.save(office);
			System.out.println("office obj is saved");
			flag=true;
		}finally {
			if(transcation != null) {
				if(flag) {
					transcation.commit();
				}else {
					transcation.rollback();
				}
			}
		}

	}

}
