package com.fetch.entity;

import java.io.Serializable;
import java.util.Set;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Office implements Serializable{
	
	private static final long serialVersionUID = -6795515791081428183L;
	protected int officeCinNo;
	protected String officeName;
	protected String officeAddress;
	protected int regYear;
	protected Set<Staff> staffs;
}
