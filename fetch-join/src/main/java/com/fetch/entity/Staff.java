package com.fetch.entity;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Staff implements Serializable{
	
	
	private static final long serialVersionUID = 2528066357443544987L;
	protected int staffId;
	protected String staffFullName;
	protected String mobileNo;
	protected String address;
	protected int experiences;
	
	protected Office office;
}
