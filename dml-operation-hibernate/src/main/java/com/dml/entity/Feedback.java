package com.dml.entity;

import java.io.Serializable;

import lombok.Data;

@Data
public class Feedback implements Serializable {
	

	private static final long serialVersionUID = -5181396184476739889L;
	private int feedBackNo;
	private String feedBackName;
	private String feedBackMessage;
	private float rating;
	
	
}
