package com.dml.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.dml.entity.Feedback;
import com.dml.helper.SessionFactoryRegistry;

public class FeedBackDao {

	public int saveFeedBack(Feedback feedback) {
		int feedBackNo=0;
		SessionFactory sessionFactory=null;
		Session session=null;
		Transaction transaction=null;
		boolean flag=false;
		try {
			sessionFactory=SessionFactoryRegistry.getInstance();
			session=sessionFactory.openSession();
			transaction=session.beginTransaction();

			feedBackNo=(int) session.save(feedback);

			flag=true;
		}finally {
			if(transaction!=null) {
				if(flag) {
					transaction.commit();
				}else {
					transaction.rollback();
				}
			}
		}
		return feedBackNo;	
	}

	public void persistFeedBack(Feedback feedback) {
		int feedBackNumber=0;
		SessionFactory sessionFactory=null;
		Session session=null;
		Transaction transaction=null;
		boolean flag=false;
		try {
			sessionFactory=SessionFactoryRegistry.getInstance();
			session=sessionFactory.openSession();
			transaction=session.beginTransaction();

			session.persist(feedback);
			feedBackNumber=feedback.getFeedBackNo();

			System.out.println("feedback no  is :: " +feedBackNumber);

			flag=true;
		}finally {
			if(transaction!=null) {
				if(flag) {
					transaction.commit();
				}else {
					transaction.rollback();
				}
			}
		}
	}

	//it deppends un-saved value . if it is 0 then it is insert otherwise fails..
	//(doubt)
	public void saveOrUpdateFeedBack(Feedback feedback) {
		int feedBackNumber=0;
		SessionFactory sessionFactory=null;
		Session session=null;
		Transaction transaction=null;
		boolean flag=false;
		try {
			sessionFactory=SessionFactoryRegistry.getInstance();
			session=sessionFactory.openSession();
			transaction=session.beginTransaction();

			session.saveOrUpdate(feedback);
			feedBackNumber=feedback.getFeedBackNo();

			System.out.println("feedback no  is :: " +feedBackNumber);

			flag=true;
		}finally {
			if(transaction!=null) {
				if(flag) {
					transaction.commit();
				}else {
					transaction.rollback();
				}
			}
		}
	}

	//still work is pending...

	public int updateFeedBack(Feedback feedback) {
		int feedBackNumber=0;
		SessionFactory sessionFactory=null;
		Session session=null;
		Transaction transaction=null;
		boolean flag=false;
		Feedback e_feedBack=null;
		try {
			sessionFactory=SessionFactoryRegistry.getInstance();
			session=sessionFactory.openSession();
			transaction=session.beginTransaction();

			e_feedBack=session.get(Feedback.class, feedback.getFeedBackNo());
			System.out.println(e_feedBack.getFeedBackNo());

			feedback.setFeedBackName(e_feedBack.getFeedBackName());
			feedback.setRating(e_feedBack.getRating());

			session.update(feedback);

			flag=true;
		}finally {
			if(transaction!=null) {
				if(flag) {
					transaction.commit();
				}else {
					transaction.rollback();
				}
			}
		}
		return feedBackNumber;
	}


	public void deleteFeedBack(Feedback feedback) {
		int feedBackNumber=0;
		SessionFactory sessionFactory=null;
		Session session=null;
		Transaction transaction=null;
		boolean flag=false;
		Feedback e_feedback=null;
		try {
			sessionFactory=SessionFactoryRegistry.getInstance();
			session=sessionFactory.openSession();
			transaction=session.beginTransaction();
			e_feedback=session.get(Feedback.class, feedback.getFeedBackNo());

			session.delete(e_feedback);
			System.out.println("feedback no  is :: " +feedBackNumber);

			flag=true;
		}finally {
			if(transaction!=null) {
				if(flag) {
					transaction.commit();
				}else {
					transaction.rollback();
				}
			}
		}
	}
}