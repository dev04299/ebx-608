package com.saveVsLoad.entity;

import java.io.Serializable;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
@Setter
@Getter
@EqualsAndHashCode
@ToString
public final class Product implements Serializable{

	private static final long serialVersionUID = 4989263412696628505L;
	private int productNo;
	private String productName;
	private String description;
	private float price;
	
}
