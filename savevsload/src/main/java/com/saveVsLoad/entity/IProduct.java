package com.saveVsLoad.entity;

public interface IProduct {
	
	public int getProductNo() ;
	public void setProductNo(int productNo);
	public String getProductName();
	public void setProductName(String productName); 
	public String getDescription(); 
		
	public void setDescription(String description); 
		
	public float getPrice(); 
	public void setPrice(float price); 

}
