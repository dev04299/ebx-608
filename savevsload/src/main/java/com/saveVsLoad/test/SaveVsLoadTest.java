package com.saveVsLoad.test;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.saveVsLoad.entity.Product;
import com.saveVsLoad.helper.SessionFactoryRegistry;

import javassist.tools.rmi.ObjectNotFoundException;

public class SaveVsLoadTest {
	public static void main(String[] args) {
		Session session=null;
		SessionFactory sessionFactory=null;
		Product product=null;
		
		sessionFactory=SessionFactoryRegistry.getSessionFactory();
		session=sessionFactory.openSession();
		try {
			 product=session.load(Product.class, 1);
		}catch(Exception ne) {
			System.out.println("****************null pointer exception***************" +ne);
		}
		
		/*
		 * try { product=session.load(Product.class, 13); }catch(ObjectNotFoundException
		 * one) { System.out.
		 * println("****************Object not found Exception ***************" +one); }
		 */
		
		System.out.println("now perform some operation on attribute...");
		System.out.println("product no :: " +product.getProductNo()); 
		/*
		 * here in the above when we fetch the productno which is a primary key of the product
		 * table so at the time of call load(), the proxy object will not go to database, rather it
		 * will provide the productNo in which it is already generated.
		 */
		System.out.println("when we fetch the on a non-primary-key-column");
		System.out.println("product name :: " +product.getProductName()); 

		/*
		 * here in the above when we fetch the productname which is not a primary key of
		 * the product table so at the time of call load(), the proxy object will go to
		 * database, collect all the data and store inside it and whenevr we call a non
		 * primary key again it will not go to database again rather it will return the
		 * fetched the stored data inside it rather it will provide the productNo in
		 * which it is already generated.
		 */

		System.out.println(product.getClass().getCanonicalName());

	}
}
