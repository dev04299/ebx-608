package com.bs3.entities;

import java.io.Serializable;

public class Bike implements Serializable{
	
	
	//private static final long serialVersionUID = 2745706128696888781L;
	protected int bikeNo;
	protected String bikeName;
	protected String type;
	protected String company;
	protected int warrantyPeriod;
	
	//generating getter and setter method
	
	public int getBikeNo() {
		return bikeNo;
	}
	public void setBikeNo(int bikeNo) {
		this.bikeNo = bikeNo;
	}
	public String getBikeName() {
		return bikeName;
	}
	public void setBikeName(String bikeName) {
		this.bikeName = bikeName;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public int getWarrantyPeriod() {
		return warrantyPeriod;
	}
	public void setWarrantyPeriod(int warrantyPeriod) {
		this.warrantyPeriod = warrantyPeriod;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bikeName == null) ? 0 : bikeName.hashCode());
		result = prime * result + bikeNo;
		result = prime * result + ((company == null) ? 0 : company.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		result = prime * result + warrantyPeriod;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Bike other = (Bike) obj;
		if (bikeName == null) {
			if (other.bikeName != null)
				return false;
		} else if (!bikeName.equals(other.bikeName))
			return false;
		if (bikeNo != other.bikeNo)
			return false;
		if (company == null) {
			if (other.company != null)
				return false;
		} else if (!company.equals(other.company))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		if (warrantyPeriod != other.warrantyPeriod)
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Bike [bikeNo=" + bikeNo + ", bikeName=" + bikeName + ", type=" + type + ", company=" + company
				+ ", warrantyPeriod=" + warrantyPeriod + "]";
	}
	
	
	
	

}
