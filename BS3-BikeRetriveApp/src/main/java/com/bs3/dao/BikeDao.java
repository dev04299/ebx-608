package com.bs3.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.bs3.entities.Bike;
import com.bs3.helper.ProgramaticApproachSessionFactoryRegistryImpl;
import com.bs3.helper.PropertiesApproachSessionFactoryRegistryImpl;
import com.bs3.helper.SessionFactoryRegistry;
import com.bs3.helper.XMLBasedApproachSessionFactoryRegistryImpl;

public class BikeDao {

	public Bike getBike(int bikeNo) {
		SessionFactoryRegistry sessionFactoryRegistry=null;
		SessionFactory sessionFactory=null;
		Session session=null;
		Bike bike=null;
		
		try {
			System.out.println("bike dao");
			//sessionFactoryRegistry=new ProgramaticApproachSessionFactoryRegistryImpl();
			//sessionFactoryRegistry=new PropertiesApproachSessionFactoryRegistryImpl();
			sessionFactoryRegistry=new XMLBasedApproachSessionFactoryRegistryImpl();
			sessionFactory=sessionFactoryRegistry.getSessionFactory();
			session=sessionFactory.openSession();
			bike=session.get(Bike.class, bikeNo);
			System.out.println("bike dao out ");
			
		}finally {
			if(session!=null) {
				session.close();
			}
		}
		return bike;
	}
}
