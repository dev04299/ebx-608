package com.bs3.helper;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class PropertiesApproachSessionFactoryRegistryImpl implements SessionFactoryRegistry{

	private static SessionFactory sessionFactory=null;
	
	static {
		Configuration configuration=new Configuration();
		System.out.println("configuration-programatic approach");
		configuration.addResource("com/bs3/entities/Bike.hbm.xml");
		System.out.println("configuration-programatic approach-adding resources...");
		sessionFactory=configuration.buildSessionFactory();
	}
	
	@Override
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	@Override
	public void closeSessionfactory() {
		if(sessionFactory!=null) {
			sessionFactory.close();
		}
		
	}
	

}
