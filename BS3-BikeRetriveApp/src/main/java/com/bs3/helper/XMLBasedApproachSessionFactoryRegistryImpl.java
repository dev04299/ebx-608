package com.bs3.helper;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class XMLBasedApproachSessionFactoryRegistryImpl implements SessionFactoryRegistry{

	private static SessionFactory sessionFactory=null;
	static {
		Configuration configuration=new Configuration().configure("com/bs3/common/hibernate.cfg.xml");
		sessionFactory=configuration.buildSessionFactory();
	}
	
	@Override
	public SessionFactory getSessionFactory() {
		// TODO Auto-generated method stub
		System.out.println("***************************************************");
		return sessionFactory;
	}

	@Override
	public void closeSessionfactory() {
		if(sessionFactory!=null) {
			sessionFactory.close();
		}
		
	}
	
	

}
