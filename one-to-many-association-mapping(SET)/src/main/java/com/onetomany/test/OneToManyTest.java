package com.onetomany.test;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import com.onetomany.dao.ProductDao;
import com.onetomany.dao.ReviewDao;
import com.onetomany.entity.Product;
import com.onetomany.entity.Review;

public class OneToManyTest {

	public static void main(String[] args) {
		ProductDao productDao=null;
		ReviewDao reviewDao=null;
		Product product=null;
		Review review1=null;
		Review review2=null;
		Set<Review> productReviews=null;
		
		productDao=new ProductDao();
		reviewDao=new ReviewDao();
		
		review1=new Review();
		review1.setRating(5);
		review1.setComments("very nice product...");
		review1.setReviewedBy("debaraj");
		review1.setRiviewPostedDate(new Date());
		int review1No=reviewDao.saveReview(review1);
		System.out.println("review no is :: " +review1No);
			
		review2=new Review();
		review2.setRating(1);
		review2.setComments("worst product...");
		review2.setReviewedBy("madhav");
		review2.setRiviewPostedDate(new Date());
		int review2No=reviewDao.saveReview(review2);
		System.out.println("review no is :: " +review2No);
		
		product=new Product();
		product.setProductName("Mobile");
		product.setManufacture("REAL ME S23");
		product.setAmount(7500);
		product.setDescription("it is an android phone");
		
		productReviews=new HashSet<>();
		productReviews.add(review1);
		productReviews.add(review2);
		product.setReviews(productReviews);
	
		int productNo=productDao.saveProduct(product);
		System.out.println("product No is  :: " +productNo);
	}

}
