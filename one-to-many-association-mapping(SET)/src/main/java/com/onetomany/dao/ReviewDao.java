package com.onetomany.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.onetomany.entity.Review;
import com.onetomany.helper.SessionFactoryRegistry;

public class ReviewDao {
	public int saveReview(Review review) {
		int reviewNo=0;
		SessionFactory sessionFactory=null;
		Session session=null;
		boolean flag=false;
		Transaction transaction=null;

		try {
			sessionFactory=SessionFactoryRegistry.getInstance();
			session=sessionFactory.openSession();
			transaction=session.beginTransaction();

			reviewNo=(int) session.save(review);

			flag=true;
		}finally {
			if(transaction != null) {
				if(flag) {
					transaction.commit();
				}else {
					transaction.rollback();
				}	
			}
			if(session !=null) {
				session.close();	
			}
		}
		return reviewNo;
	}

	public Review fetch(int reviewNo) {
		Review review=null;
		SessionFactory sessionFactory=null;
		Session session = null;
		Transaction transaction=null;
		boolean flag=false;

		try {
			sessionFactory=SessionFactoryRegistry.getInstance();
			session=sessionFactory.openSession();
			transaction=session.beginTransaction();
			review=session.get(Review.class, reviewNo);
			flag=true;
		}finally {
			if(transaction != null) {
				if(flag) {
					transaction.commit();
				}else {
					transaction.rollback();
				}
			}
			if(session !=null) {
				session.close();	
			}
		}
		return review;
	}

}
