package com.onetomany.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.onetomany.entity.Product;
import com.onetomany.helper.SessionFactoryRegistry;

public class ProductDao {

	public int saveProduct(Product product) {
		int productNo=0;
		SessionFactory sessionFactory=null;
		Session session=null;
		boolean flag=false;
		Transaction transaction=null;

		try {
			sessionFactory=SessionFactoryRegistry.getInstance();
			session=sessionFactory.openSession();
			transaction=session.beginTransaction();

			productNo=(int) session.save(product);

			flag=true;
		}finally {
			if(transaction != null) {
				if(flag) {
					transaction.commit();
				}else {
					transaction.rollback();
				}
			}
			if(session !=null) {
				session.close();	
			}
		}
		return productNo;
	}
	public Product fetchProduct(int productNo) {
		Product product=null;
		SessionFactory sessionFactory=null;
		Session session=null;
		boolean flag=false;
		Transaction transaction=null;

		try {
			sessionFactory=SessionFactoryRegistry.getInstance();
			session=sessionFactory.openSession();
			transaction=session.beginTransaction();

			product=session.get(Product.class, productNo);
			flag=true;
		}finally {
			if(transaction != null) {
				if(flag) {
					transaction.commit();
				}else {
					transaction.rollback();
				}
			}
			if(session !=null) {
				session.close();	
			}
		}
		return product;
	}

}
