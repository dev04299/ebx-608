package com.nt.test;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Test {

	public static void main(String[] args) throws ClassNotFoundException, NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		String attributeNames[]=new String[] {"accountNo", "accountName"};
		String attributeData[]=new String[] {"1", "warn"};
		Class<?> accountClass=Class.forName("com.nt.bean.Account");
		
		Object obj=accountClass.newInstance();
		//Class clazz=Class.forName("Account");
		for(int i=0;i<attributeNames.length;i++) {
			String attributeName=attributeNames[i];
			
			String setterName="set"+attributeName.toUpperCase().charAt(0)+attributeName.substring(1, attributeName.length());
			Method method=accountClass.getDeclaredMethod(setterName, String.class);
			method.invoke(obj, attributeData[i]);
			
		}
		System.out.println(obj);
	}
}
