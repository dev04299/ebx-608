package com.nt.bean;

public class Account {

	private String accountNo;
	private String accountName;
	public String getAccountNo() {
		return accountNo;
	}
	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}
	public String getAccountName() {
		return accountName;
	}
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
	@Override
	public String toString() {
		return "Account [accountNo=" + accountNo + ", accountName=" + accountName + "]";
	}
	
	
}
