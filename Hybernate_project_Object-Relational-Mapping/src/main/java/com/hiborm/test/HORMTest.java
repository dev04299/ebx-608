package com.hiborm.test;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.hiborm.enity.Account;

public class HORMTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//Configuration configuration=new Configuration().configure();
		Configuration configuration=new Configuration().configure();
		SessionFactory sessionFactory=configuration.buildSessionFactory();

		Session session=sessionFactory.openSession();
		Account account=session.get(Account.class,1);
		System.out.println(account);
	}

}
