package com.jpa.cpk.annotation.test;

import com.jpa.cpk.annotation.dao.PersionalDao;
import com.jpa.cpk.annotation.entity.CompositePrimaryKey;
import com.jpa.cpk.annotation.entity.PersionalData;

public class JpaCompositeAnnotationTest {

	public static void main(String[] args) {
		PersionalDao dao=new PersionalDao();
		CompositePrimaryKey com=new CompositePrimaryKey();
		com.setDistrictName("khurdha");
		com.setStateName("odisha");
		
		PersionalData data=new PersionalData();
		data.setCompositePrimaryKey(com);
		data.setPersionId(123);
		data.setPersionName("DEBARAJ MAHAPATRA");
		data.setMobileNo("63715498241");
		data.setAddress("BBSR");
		data.setStatus("you are eligible...");
		
		dao.savePersionalInformation(data);
		System.out.println("successfully Register");

	}

}
