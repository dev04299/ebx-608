package com.jpa.cpk.annotation.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Entity;

import lombok.Data;

@Data
@Embeddable
public class CompositePrimaryKey implements Serializable{
	
	@Column(name = "state_nm")
	private String stateName;
	@Column(name = "district_nm")
	private String districtName;
}
