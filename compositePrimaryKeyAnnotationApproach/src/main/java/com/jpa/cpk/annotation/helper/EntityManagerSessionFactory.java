package com.jpa.cpk.annotation.helper;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class EntityManagerSessionFactory {
	private static EntityManagerFactory emf;
	
	static {
		emf=Persistence.createEntityManagerFactory("persional-pu");
	}
	public static EntityManagerFactory getInstnce() {
		return emf;
	}
	public static void close() {
		if(emf!=null) {
			emf.close();
		}
	}
}
