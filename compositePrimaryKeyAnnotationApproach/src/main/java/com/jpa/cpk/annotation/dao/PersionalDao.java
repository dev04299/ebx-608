package com.jpa.cpk.annotation.dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;

import com.jpa.cpk.annotation.entity.PersionalData;
import com.jpa.cpk.annotation.helper.EntityManagerSessionFactory;

public class PersionalDao {

	public void savePersionalInformation(PersionalData data) {
		boolean flag=false;
		EntityTransaction transaction=null;
		EntityManagerFactory emf=null;
		EntityManager em=null;
		
		try {
			emf=EntityManagerSessionFactory.getInstnce();
			em=emf.createEntityManager();
			transaction=em.getTransaction();
			System.out.println("&*&*&*&*&*^(");
			transaction.begin();
			em.persist(data);
			flag=true;
		}finally {
			if(transaction!=null) {
				if(flag) {
					transaction.commit();
				}else {
					transaction.rollback();
				}
			}
			if(em!=null) {
				em.close();
			}
		}
	}
}
