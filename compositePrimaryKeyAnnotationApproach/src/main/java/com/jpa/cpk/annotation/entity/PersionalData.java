package com.jpa.cpk.annotation.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Data;

@Data
@Table(name = "persional")
@Entity
public class PersionalData implements Serializable {
	
	@Embedded
	private CompositePrimaryKey compositePrimaryKey;
	@Column(name = "persion_id")
	private int persionId;
	@Column(name = "persion_nm")
	private String persionName;
	@Column(name = "address")
	private String address;
	@Column(name = "mobile_no")
	private String mobileNo;
	private String status;
}
