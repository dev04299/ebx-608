package com.inverse.test;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.inverse.entity.Member;
import com.inverse.entity.Project;
import com.inverse.helper.SessionFactoryRegistry;


public class InvTest {

	public static void main(String[] args) {
		SessionFactory sessionFactory=null;
		Session session=null;
		Transaction transaction=null;
		boolean flag=false;

		Project project=null;
		Member member=null;
		Set<Member> setOfMembers=null;
		try {
			sessionFactory=SessionFactoryRegistry.getInstance();
			session=sessionFactory.openSession();
			transaction=session.beginTransaction();

			project=new Project();
		
			project.setProjectName("Jeevan Dhara project");
			project.setStartDate(new Date());
			project.setExperiences(4);

			member=new Member();
		
			member.setMemberName("DEBARAJ");
			member.setMobileNo("8908923881");
			member.setQualification("B.TECT");
			member.setAddress("Rasulgard, BBSR");
			member.setProject(project);

			setOfMembers=new HashSet<Member>();
			setOfMembers.add(member);
			project.setMembers(setOfMembers);

			session.save(project);
			session.save(member);

			flag=true;

		}finally {
			if(transaction!=null) {
				if(flag) {
					transaction.commit();
				}else {
					transaction.rollback();
				}
			}
			if(session!=null) {
				session.close();
			}
		}


	}

}
