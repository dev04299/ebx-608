package com.inverse.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Project implements Serializable{
	

	private static final long serialVersionUID = 6087230702606964280L;
	protected int projectNo;
	protected String projectName;
	protected Date startDate;
	protected int experiences;
	Set<Member> members;
	
}
