package com.inverse.entity;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Member implements Serializable {
	
	private static final long serialVersionUID = -3708192609372195243L;
	protected int memberNo;
	protected String memberName;
	protected String mobileNo;
	protected String address;
	protected String qualification;
	
	protected Project project;
	
	
}
