package com.nt.singletone;

public class Score {
	private static Score instance;
	private ThreadLocal<Integer> localThreadScore;
	int score=0;
	
	private Score() {
		localThreadScore=new ThreadLocal<Integer>();
	}
	public static Score getInstance() {
		if(instance==null) {
			instance=new Score();
		}
		return instance;
	}
	
	public void addScore(int points) {	
		if(localThreadScore.get()==null) {
			score +=points;
			localThreadScore.set(score);
		}else {
			localThreadScore.get();
		}
		
	}
	public void showScore() {
		System.out.println("Score is :: " +score);
	}
}
