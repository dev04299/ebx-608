package com.bs4.initilizer;

import java.util.Map;

import org.hibernate.engine.jdbc.connections.internal.ConnectionProviderInitiator;
import org.hibernate.engine.jdbc.connections.spi.ConnectionProvider;
import org.hibernate.service.spi.ServiceRegistryImplementor;

import com.bs4.service.OurOwnConnectionServiceProviderImpl;

public class OurOwnServiceInititor extends ConnectionProviderInitiator{

	@Override
	public ConnectionProvider initiateService(Map configurationValues, ServiceRegistryImplementor registry) {
		System.out.println("OurOwnServiceInititor.initiateService()--------------");
		return new OurOwnConnectionServiceProviderImpl();
	}

}
