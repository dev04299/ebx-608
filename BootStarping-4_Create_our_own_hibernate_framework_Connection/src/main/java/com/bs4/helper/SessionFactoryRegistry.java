package com.bs4.helper;

import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataBuilder;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.SessionFactoryBuilder;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

public class SessionFactoryRegistry {

	private static SessionFactory sessionFactory;
	static {
		/*
		 * Simple method
		 * 
		 * Configuration configuration=new Configuration().configure();
		 * sessionFactory=configuration.buildSessionFactory();
		 */


		//This is BootStraping-4 Logic

		/*
		 * Configuration configuration=new Configuration();
		 * StandardServiceRegistryBuilder builder=new
		 * StandardServiceRegistryBuilder().configure(); StandardServiceRegistry
		 * registry=builder.build();
		 * sessionFactory=configuration.buildSessionFactory(registry);
		 */


		/*
		 * Configuration configuration=new Configuration();
		 * StandardServiceRegistryBuilder builder=new
		 * StandardServiceRegistryBuilder().configure().addInitiator(new
		 * OurOwnServiceInititor()); StandardServiceRegistry registry=builder.build();
		 * sessionFactory=configuration.buildSessionFactory(registry);
		 */
		
		StandardServiceRegistryBuilder builder=new StandardServiceRegistryBuilder().configure();
		//add initiator
		StandardServiceRegistry registry=builder.build();
		
		MetadataSources metadataSources=new MetadataSources(registry);
		MetadataBuilder metadataBuilder=metadataSources.getMetadataBuilder();
		Metadata metadata=metadataBuilder.build();
		SessionFactoryBuilder sessionFactoryBuilder=metadata.getSessionFactoryBuilder();
		
		sessionFactoryBuilder.applyAutoClosing(true);
		sessionFactory=sessionFactoryBuilder.build();
		System.out.println(sessionFactory);
	}

	public static SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public static void closeSessionFactory() {
		if(sessionFactory!=null) {
			sessionFactory.close();
		}
	}
}
