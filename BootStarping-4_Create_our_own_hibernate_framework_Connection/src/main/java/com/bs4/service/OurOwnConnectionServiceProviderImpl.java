package com.bs4.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Map;

import org.hibernate.HibernateException;
import org.hibernate.engine.jdbc.connections.spi.ConnectionProvider;
import org.hibernate.service.spi.Configurable;
import org.hibernate.service.spi.Startable;
import org.hibernate.service.spi.Stoppable;

public class OurOwnConnectionServiceProviderImpl implements ConnectionProvider, Configurable, Startable, Stoppable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2706795164207030057L;
	private String driverClassName;
	private String url;
	private String username;
	private String password;
	@Override
	public boolean isUnwrappableAs(Class unwrapType) {

		return true;
	}
	@Override
	public <T> T unwrap(Class<T> unwrapType) {
		System.out.println("OurOwnConnectionServiceProviderImpl.unwrap()");
		return null;
	}
	@Override
	public void stop() {
		System.out.println("OurOwnConnectionServiceProviderImpl.stop()");

		driverClassName=null;
		url=null;
		username=null;
		password=null;

	}
	@Override
	public void start() {
		try {
			Class.forName(driverClassName);
		} catch (ClassNotFoundException e) {
			System.out.println("*******************************Exception ************");
			throw new HibernateException("error while loading driver class :: " +e);
		}

	}


	@Override
	public void configure(Map configurationValues) {
		System.out.println("OurOwnConnectionServiceProviderImpl.configure()");
		/*
		 * driverClassName=(String) configurationValues.get("connection.driver_class");
		 * System.out.println(driverClassName); url=(String)
		 * configurationValues.get("connection.url"); System.out.println(url);
		 * username=(String) configurationValues.get("connection.username");
		 * System.out.println(username); password=(String)
		 * configurationValues.get("connection.password"); System.out.println(password);
		 */
		driverClassName="com.mysql.cj.jdbc.Driver";
		url="jdbc:mysql:///hybernatedb";
		username="root";
		password="root";

	}
	@Override
	public Connection getConnection() throws SQLException {
		System.out.println("OurOwnConnectionServiceProviderImpl.getConnection()");
		Connection con=DriverManager.getConnection(url, username, password);
		con.setAutoCommit(false);
		return con;
	}
	@Override
	public void closeConnection(Connection conn) throws SQLException {
		System.out.println("OurOwnConnectionServiceProviderImpl.closeConnection()");

		if(conn!=null && conn.isClosed()==false) {
			conn.close();
		}

	}
	@Override
	public boolean supportsAggressiveRelease() {
		// TODO Auto-generated method stub
		return true;
	}



}
