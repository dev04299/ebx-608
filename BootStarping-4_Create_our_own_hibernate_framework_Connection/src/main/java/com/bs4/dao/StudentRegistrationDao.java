package com.bs4.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.bs4.enity.StudentRegistration;
import com.bs4.helper.SessionFactoryRegistry;

public class StudentRegistrationDao {

	public StudentRegistration getStudentDetails(int studentNo) {
		SessionFactory sessionFactory=null;
		Session session=null;
		StudentRegistration studentRegistration=null;
		try {
			sessionFactory=SessionFactoryRegistry.getSessionFactory();
			session=sessionFactory.openSession();
			studentRegistration=session.get(StudentRegistration.class, studentNo);
		}finally {
			if(session!=null) {
				session.close();
			}
		}
		return studentRegistration;
	}
}
