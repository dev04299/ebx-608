package com.onetomany.entity;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

@Data
public class Review implements Serializable{
	
	private static final long serialVersionUID = 5433757204102374765L;
	protected int reviewNo;
	protected int rating;
	protected String comments;
	protected Date riviewPostedDate;
	protected String reviewedBy;
	protected Product product;
	
	
}
