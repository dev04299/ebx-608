package com.onetomany.entity;

import java.io.Serializable;

import lombok.Data;


@Data
public class Product implements Serializable{
	
	
	private static final long serialVersionUID = -230806445632130216L;
	protected int productNo;
	protected String productName;
	protected String description;
	protected String manufacture;
	protected double amount;
	

}
