package com.onetomany.test;

import java.util.Date;

import com.onetomany.dao.ProductDao;
import com.onetomany.dao.ReviewDao;
import com.onetomany.entity.Product;
import com.onetomany.entity.Review;

public class ManyToOneTest {

	public static void main(String[] args) {
		ProductDao productDao=null;
		ReviewDao reviewDao=null;
		Product product=null;
		Review review1=null;
		Review review2=null;

		productDao=new ProductDao();
		reviewDao=new ReviewDao();

		product=new Product();
		product.setProductName("Mobile");
		product.setManufacture("REAL ME S23");
		product.setAmount(7500);
		product.setDescription("it is an android phone");
		
		int productNo=productDao.saveProduct(product);
		System.out.println("product No is  :: " +productNo);


		review1=new Review();
		review1.setRating(5);
		review1.setComments("very nice product...");
		review1.setReviewedBy("debaraj");
		review1.setRiviewPostedDate(new Date());
		review1.setProduct(product);
		
		int review1No=reviewDao.saveReview(review1);
		System.out.println("review no is :: " +review1No);

		review2=new Review();
		review2.setRating(1);
		review2.setComments("worst product...");
		review2.setReviewedBy("madhav");
		review2.setRiviewPostedDate(new Date());
		review2.setProduct(product);
		int review2No=reviewDao.saveReview(review2);
		System.out.println("review no is :: " +review2No);





	}

}
