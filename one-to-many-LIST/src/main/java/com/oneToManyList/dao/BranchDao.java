package com.oneToManyList.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.oneToManyList.entity.Branch;
import com.oneToManyList.helper.SessionFactoryRegistry;

public class BranchDao {

	public int saveBranch(Branch branch) {
		int branchNo=0;
		SessionFactory sessionFactory=null;
		Session session=null;
		Transaction transaction=null;
		boolean flag=false;
		
		try {
			sessionFactory=SessionFactoryRegistry.getInstance();
			session=sessionFactory.openSession();
			transaction=session.beginTransaction();
			
			branchNo=(int) session.save(branch);
			flag=true;
		}finally {
			if(transaction != null) {
				if(flag) {
					transaction.commit();
				}else {
					transaction.rollback();
				}
			}
		}
		
		
		return branchNo;
	}
}
