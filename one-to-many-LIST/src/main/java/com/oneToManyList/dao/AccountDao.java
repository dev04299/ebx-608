package com.oneToManyList.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.oneToManyList.entity.Account;
import com.oneToManyList.helper.SessionFactoryRegistry;

public class AccountDao {
	
	public int saveAccount(Account account) {
		int accountNo=0;
		SessionFactory sessionFactory=null;
		Session session=null;
		Transaction transaction=null;
		boolean flag=false;
		
		try {
			sessionFactory=SessionFactoryRegistry.getInstance();
			session=sessionFactory.openSession();
			transaction=session.beginTransaction();
			
			accountNo=(int) session.save(account);
			flag=true;
		}finally {
			if(transaction != null) {
				if(flag) {
					transaction.commit();
				}else {
					transaction.rollback();
				}
			}
		}
		
		return accountNo;
	}

}
