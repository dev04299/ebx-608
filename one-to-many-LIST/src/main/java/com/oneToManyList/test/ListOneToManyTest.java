package com.oneToManyList.test;

import java.util.ArrayList;
import java.util.List;

import com.oneToManyList.dao.AccountDao;
import com.oneToManyList.dao.BranchDao;
import com.oneToManyList.entity.Account;
import com.oneToManyList.entity.Branch;

public class ListOneToManyTest {

	public static void main(String[] args) {
		BranchDao branchDao=null;
		AccountDao accountDao=null;
		Account account1=null;
		Account account2=null;
		Branch branch=null;
		List<Account> accountList=null;
		
		branchDao=new BranchDao();
		accountDao=new AccountDao();
		
		account1=new Account();
		account1.setAccountHolderName("debaraj mahapatra");
		account1.setIfscCode("SBI-04299");
		account1.setPhoneNo(993784401);
		account1.setAddress("Rasulgard");
		
		account2=new Account();
		account2.setAccountHolderName("madhav ranjan jena");
		account2.setIfscCode("SBI-0234");
		account2.setPhoneNo(862898913);
		account2.setAddress("Baramunda");
		
		branch=new Branch();
		branch.setBranchName("SMI-JHARAPADA");
		branch.setAddress("BBSR-JHARAPADA");
		branch.setContactNo(99339538);
	
		accountList=new ArrayList<Account>();
		accountList.add(account1);
		accountList.add(account2);
		branch.setAssignedAccounts(accountList);
		
		accountDao.saveAccount(account1);
		accountDao.saveAccount(account2);
		int branchNo=branchDao.saveBranch(branch);
		System.out.println(branchNo);	
		
	}
}
