package com.hyberexam2.dao;



import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.hyberexam2.entities.Branch;
import com.hyberexam2.helper.SessionFactoryRegistry;


public class BranchDao {
	SessionFactoryRegistry sessionFactoryRegistry;


	public Branch getBranch(int branchNo) {

		SessionFactory sessionFactory=null;
		Session session=null;
		Branch branch=null;
		Transaction1 transaction1=null;
		Transaction transaction=null;
		boolean flag=false;
		try {
			sessionFactory=SessionFactoryRegistry.newSessionFactory();
			session=sessionFactory.openSession();
			transaction=session.beginTransaction();	

			branch=session.get(Branch.class, 2);
			flag=true;
		}finally {
			if(transaction!=null) {
				if(flag) {
					transaction.commit();
				}else {
					transaction.rollback();
				}
			}
		}

		return branch;
	}

}
