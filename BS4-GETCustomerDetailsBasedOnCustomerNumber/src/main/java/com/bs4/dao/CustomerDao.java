package com.bs4.dao;

import java.util.Date;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.bs4.entities.Customer;
import com.bs4.helper.SessionFactoryRegistry;

public class CustomerDao {

	public Customer getCustomer(int customerNo) {
		Session session=null;
		Customer customer=null;
		SessionFactory sessionFactory=null;
		Transaction transaction=null;
		boolean flag=false;
		try {
			sessionFactory=SessionFactoryRegistry.getSessionfactory();
			session=sessionFactory.openSession();
			session.beginTransaction();
			//customer=session.get(Customer.class, customerNo);
			customer=new Customer();
			customer.setCustomerNo(1);
			customer.setFullName("madhav ranjan jena");
			customer.setDob(new Date());
			customer.setDrivingLicenceNo("ABCD3263");
			customer.setGender("MALE");
			customer.setMobileNo("466542313");
			
			session.update(customer);
			System.out.println("CustomerDao.update()");
			flag=true;
		}finally {
			
			if(transaction!=null) {
				if(flag) {
					transaction.commit();
				}else {
					transaction.rollback();
					
				}
			}
			
			if(session!=null) {
				session.close();
			}
		}
		return customer;
		
	}
}
