package com.bs4.test;

import com.bs4.dao.CustomerDao;
import com.bs4.entities.Customer;
import com.bs4.helper.SessionFactoryRegistry;

public class BS4Test {

	public static void main(String[] args) {

		try {
			CustomerDao dao=new CustomerDao();
			Customer customer=dao.getCustomer(1);
			System.out.println(customer);
		}finally {
			SessionFactoryRegistry.closeSessionFactory();
		}
	}

}
