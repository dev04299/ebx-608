package com.bs4.entities;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class Customer {
	
	private int customerNo;
	private String fullName;
	private Date dob;
	private String gender;
	private String mobileNo;
	private String emailID;
	private String drivingLicenceNo;
	
	

}
